namespace ProjetTest.test;

[TestClass]
public class CalculatriceTests
{
    [TestMethod]
    public void CallingAdditionWith3And5ShouldReturn8()
    {
        var calculatrice = new Calculatrice();
        var result = calculatrice.Sum(3, 5);
        var result2 = calculatrice.Division(10, 2);
        var result3 = calculatrice.Subtracion(4, 2);
        Assert.AreEqual(8, result); // résultat attendu 8
        Assert.AreEqual(5, result2); // résultat attendu 5
        Assert.AreEqual(2, result3); // résultat attendu 2
    }
    
    [TestMethod]
    public void CallingDivisionWith10And5ShouldReturn5()
    {
        var calculatrice = new Calculatrice();
        var result = calculatrice.Division(10, 2);
        Assert.AreEqual(5, result); // résultat attendu 5
    }
    
    [TestMethod]
    public void CallingSubtractionWith4And2ShouldReturn2()
    {
        var calculatrice = new Calculatrice();
        var result = calculatrice.Subtracion(4, 2);
        Assert.AreEqual(2, result); // résultat attendu 2
    }
}