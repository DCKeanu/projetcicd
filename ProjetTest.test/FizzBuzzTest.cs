namespace ProjetTest.test;

[TestClass]
public class FizzBuzzTest
{
    [TestMethod]
    public void Calling_FizzBuzzTest_With1_Should_Return_1()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.FizzBuzzTest(1);
        Assert.AreEqual(1, result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzzTest_With3_Should_Return_Fizz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.FizzBuzzTest(3);
        Assert.AreEqual("Fizz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzzTest_With5_Should_Return_Buzz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.FizzBuzzTest(5);
        Assert.AreEqual("Buzz", result);
    }
    
    [TestMethod]
    public void Calling_FizzBuzzTest_With15_Should_Return_Buzz()
    {
        var fizzBuzz = new FizzBuzz();
        var result = fizzBuzz.FizzBuzzTest(15);
        Assert.AreEqual("FizzBuzz", result);
    }
}